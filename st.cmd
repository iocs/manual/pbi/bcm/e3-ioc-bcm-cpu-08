require essioc
require adsis8300bcm

iocshLoad("instance.iocsh")

epicsEnvSet("P",        "$(CONTROL_GROUP):")
epicsEnvSet("R",        "$(AMC_NAME):")
epicsEnvSet("PREFIX",   "$(P)$(R)")

epicsEnvSet("EVR_DEV",    "EVR-101")
epicsEnvSet("EVR_PREFIX", "$(P)Ctrl-$(EVR_DEV):")

############################################################################
#
# load BCM data acquistion
#
############################################################################

iocshLoad("$(adsis8300bcm_DIR)/bcm-main.iocsh", "CONTROL_GROUP=$(CONTROL_GROUP), AMC_NAME=$(AMC_NAME), AMC_DEVICE=$(AMC_DEVICE), EVR_NAME=$(EVR_PREFIX)")


############################################################################
#
# Load common module startup scripts (Shall be loaded at the end)
# Autosave is loaded in here
############################################################################
#epicsEnvSet("ASG_FILENAME", "bcm_security.acf")
iocshLoad("$(essioc_DIR)common_config.iocsh")

#- custom autosave in order to have Lut ID PVs restored before the others
afterInit("makeAutosaveFileFromDbInfo('$(AS_TOP)/$(IOCDIR)/req/lutIDs.req','autosaveFieldsLutIDs')")
afterInit("create_monitor_set("lutIDs.req",5)")
afterInit("fdbrestore("$(AS_TOP)/$(IOCDIR)/save/lutIDs.sav")")
afterInit("system 'sleep 2'")

afterInit("fdbrestore("$(AS_TOP)/$(IOCDIR)/save/settings.sav")")

############################################################################
#
# Call iocInit to start the IOC
#
############################################################################
iocInit
## Add any post-iocInit statements here

date
